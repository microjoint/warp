#!/bin/bash

sudo apt-get install git
cd /etc
sudo git clone https://microjoint@bitbucket.org/microjoint/warp.git puppet
cd puppet
sudo git submodule init
sudo git submodule update --init
sudo git submodule update --recursive --remote

sudo apt-get install puppet

#sudo cd /etc/puppet && sudo ./rush.sh work workstation noc
