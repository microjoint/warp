#!/bin/bash
#set -x

update_repo() {
  if [[ -d "/etc/puppet/.git" ]]
  then
    cd "/etc/puppet"
    git pull
    git submodule update
  else
    echo 'cannot update. clone repo? (y/N)'
    read inupt
    if [[ $input == 'y' ]]
    then
      rm -rf /etc/puppet
      git clone --recursive https://microjoint@bitbucket.org/microjoint/warp.git /etc/puppet
    fi
  fi
}


if [ $# -ne 3 ]
then
  echo "Please supply environment(work,home) hardware(workstation,laptop,tablet) role(desktop,server,fridge)"
  exit 1
fi

#install puppet if it doesn't exist
if [[ ! -x "/usr/bin/puppet" ]]
then
  apt-get install -y puppet
fi

# update or fetch manifests
update_repo

FACTER_environment=$1 FACTER_hardware=$2 FACTER_role=$3 puppet apply --modulepath /etc/puppet/modules /etc/puppet/manifests/site.pp
#FACTER_environment=$1 FACTER_hardware=$2 FACTER_role=$3 puppet apply --modulepath /etc/puppet/modules /etc/puppet/manifests/site.pp --verbose --debug

update_repo

