#
class profile::apps
{

  $packages = lookup(profile::apps::to_install, {merge => {'strategy' => 'deep'}, default_value => undef})

  if $packages
  {
    create_resources( package, $packages )
  }

}
