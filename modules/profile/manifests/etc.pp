#
class profile::etc (
  $dns_search = lookup('dns_search', Tuple, 'unique', ''),
  $resolv_conf = lookup('resolv_conf', Tuple, 'unique', undef)
) {

  $mnt_config = lookup(profile::etc::mnt_config, { merge => {'strategy' => 'deep'}})
  $mnt_defaults = lookup(profile::etc::mnt_config_defaults, { merge => {'strategy' => 'deep'}})
  $config = lookup(profile::etc::config,{ merge => {'strategy' => 'deep'}})

  if $config {
    create_resources( file, $config )
  }

  if $mnt_config {
    create_resources( mount, $mnt_config, $mnt_defaults)
  }

  if $resolv_conf {
    file { '/etc/resolv.conf':
      ensure  => file,
      content => template('profile/resolv.conf.erb'),
    }
  }

}
