# Create the host file
class profile::hosts
{

  $hosts = lookup('hosts', {merge => 'hash', default_value => undef })

  if $hosts { create_resources(host, $hosts) }

}
