# creates a remina configuration files
define profile::remmina (
  $server = undef,
  $user   = undef,
  $pass   = undef,
  $username = undef,
) {

  file { "/home/${username}/.remmina/${name}.remmina":
    ensure  => file,
    content => template('profile/remmina.erb')
  }
}

