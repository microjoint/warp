# This is for a desktop PC that we'd have a GUI interface
class profile::mysql(
  $root_password = None,
  $options = undef,
  $restart = true,
  $users,
  $grants,
  $databases,
){

  class { '::mysql::server':
    root_password           => $root_password,
    remove_default_accounts => true,
    override_options        => $options,
    restart                 => $restart,
    users                   => $users,
    grants                  => $grants,
    databases               => $databases,
  }

}
