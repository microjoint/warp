# This is for a desktop PC that we'd have a GUI interface
class profile::desktop(
  $xrandr = lookup('xrandr', String, 'unique', nil) # command to customise screens
){
  #include lightdm
  notice('desktop')

  # set common core applications
  include git
  include davfs2
  include profile::synergy

  # setup port knocking
  # not ready yet
  #include profile::port_knocking

  # Let's have some ponysay
  vcsrepo { '/usr/local/bin/ponysay':
    ensure   => latest,
    provider => git,
    source   => 'https://github.com/erkin/ponysay.git',
    notify   => Exec['install_ponysay'],
  }

  exec { 'install_ponysay':
    command     => 'python3 setup.py --freedom=partial --without-info install',
    path        => ['/usr/bin','/bin/'],
    cwd         => '/usr/local/bin/ponysay',
    refreshonly => true,
  }

  # let's have some pokemon
  vcsrepo { '/usr/local/bin/pokemonsay':
    ensure   => latest,
    provider => git,
    source   => 'https://github.com/possatti/pokemonsay.git',
    notify   => Exec['install_pokemonsay'],
  }

  exec { 'install_pokemonsay':
    command     => './install.sh',
    path        => ['/usr/bin','/bin/'],
    cwd         => '/usr/local/bin/pokemonsay',
    refreshonly => true,
  }
}
