#
class profile::port_knocking {
  resources { 'firewall':
    purge => true,
  }

  resources { 'firewallchain':
    purge => true,
  }

  Firewall {
    before  => Class['profile::port_knocking::post'],
    require => Class['profile::port_knocking::pre'],
  }

  class { ['profile::port_knocking::pre', 'profile::port_knocking::post']: }

  class { 'firewall': }

  # see https://www.digitalocean.com/community/tutorials/how-to-configure-port-knocking-using-only-iptables-on-an-ubuntu-vps
  #create chains
  #firewallchain { 'KNOCKING:filter:IPv4':
  firewallchain { 'KNOCKING':
  }
  firewallchain { 'GATE1':
  }
  firewallchain { 'GATE2':
  }
  firewallchain { 'GATE3':
  }
  firewallchain { 'PASSED':
  }

  firewall { '100 started in state KNOCKING':
    chain => 'INPUT',
    jump  => 'KNOCKING',
  }

}
