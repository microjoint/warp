# This is for a desktop PC that we'd have a GUI interface
class profile::mongodb
{

  $databases = lookup(mongodb::databases, {merge => 'hash'}, undef)

  #class { '::mongodb::server':
  #  port => 27000,
  #}

  if $databases {
    create_resources( 'mongodb_database', $databases )
  }

}
