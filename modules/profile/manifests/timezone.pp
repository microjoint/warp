# Set the timezone
class profile::timezone
{

  $timezone = lookup('profile::timezone')

  file{ '/etc/timezone':
    ensure  => file,
    content => "${timezone}\n",
  }

}
