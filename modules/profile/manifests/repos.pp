# Install of any git repos
class profile::repos
{

  $to_clone = lookup(profile::repos::to_clone, { merge => 'hash', default_value => undef })

  if $to_clone
  {
    $defaults = {
      ensure   => latest,
      provider => git,
    }
    create_resources( vcsrepo, $to_clone, $defaults )
  }

}
