# This is for a box being used for development

class profile::devbox{

  $exec = lookup('profile::exec', {merge => 'hash'}, undef)

  # set common core applications
  include git

  if $profile::devbox::exec {
    create_resources( exec, $profile::devbox::exec )
  }

}
