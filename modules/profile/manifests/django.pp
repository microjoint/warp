# This is for a box being used for django development

class profile::django{

  class {'apache':
    default_vhost => false,
  }

  apache::vhost { 'defaut':
    servername => false,
    port       => 80,
    docroot    => '/var/www/static',
  }

  file { '/var/www/static':
    ensure  => 'lnk',
    target  => '/vagrant/src/wh_noc/static',
    owner   => 'www-data',
    group   => 'www-data',
    require => Class['apache'],
  }

}
