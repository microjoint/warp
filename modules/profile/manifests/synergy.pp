# Create synergy FW rule
class profile::synergy
{
  firewall { '300 allow synergy':
    chain  => 'INPUT',
    state  => ['NEW'],
    source => '10.180.117.234',
    dport  => 24800,
    proto  => 'tcp',
    action => 'accept',
}
  firewall { '301 allow synergy':
    chain  => 'INPUT',
    state  => ['NEW'],
    source => '192.168.2.180',
    dport  => 24800,
    proto  => 'tcp',
    action => 'accept',
  }
}
