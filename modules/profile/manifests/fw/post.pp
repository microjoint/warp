#
class profile::fw::post {

  $post_rules = lookup('profile::fw::post', {merge => {'strategy' => 'deep' }})

  if $post_rules { create_resources( firewall, $post_rules) }

}
