class profile::fw::pre {
  Firewall {
    require => undef,
  }

  # basic in/out
  $pre_rules = lookup('profile::fw::pre', {merge => {'strategy' => 'deep' }})
  if $pre_rules { create_resources( firewall, $pre_rules) }

}
