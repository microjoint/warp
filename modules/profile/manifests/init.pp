# This is the common profile applied to all machines
class profile
{

  # we setup users
  $users = lookup('users', {merge => {'strategy' => 'deep'}})
  if $users {
    create_resources( profile::user, $users)
    #notify { 'user create':
    #  name     => "users are ${users}",
    #  withpath => true,
    #}
  }

  # any global configuration
  include profile::etc

  # set common services
  include profile::fw
  include openssh
  #class { '::ntp':
  #  restrict => ['127.0.0.1'],
  #}


  # install all the applications

  include profile::apps

  # Bring on the vim
  include vim

  # repos to clone
  include profile::repos

  # set up common configuration
  $config = lookup(profile::config, {merge => 'hash', default_value => undef})
  if $config {
    create_resources( file, $config)
  }

  include profile::hosts
  include profile::timezone

  Class['vim'] -> Class['profile::repos']

}
