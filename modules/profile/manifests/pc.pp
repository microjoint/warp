#
class profile::pc inherits profile {

  $packages = lookup(profile::apps::to_install, {merge => 'hash}, undef)

  if $packages
  {
    create_resources( package, $packages )
  }

}
