#
class profile::fw {

  resources { 'firewall':
    purge => true,
  }

  Firewall {
    before  => Class['profile::fw::post'],
    require => Class['profile::fw::pre'],
  }

  class { ['::profile::fw::pre', '::profile::fw::post']: }


}
